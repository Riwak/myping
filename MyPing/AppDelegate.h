//
//  AppDelegate.h
//  MyPing
//
//  Created by Audouin d'Aboville on 20/11/2015.
//  Copyright © 2015 Audouin d'Aboville. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

