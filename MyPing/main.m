//
//  main.m
//  MyPing
//
//  Created by Audouin d'Aboville on 20/11/2015.
//  Copyright © 2015 Audouin d'Aboville. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
