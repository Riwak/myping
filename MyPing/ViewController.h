//
//  ViewController.h
//  MyPing
//
//  Created by Audouin d'Aboville on 20/11/2015.
//  Copyright © 2015 Audouin d'Aboville. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController
{
    NSString *Host;
    double delay_value;
    NSTimer *Timer;
}

@property (strong, nonatomic) NSStatusItem *statusItem;

- (void)MAINCONTROLLER:(id)timer;

@property (weak, nonatomic) IBOutlet NSTextField *host;
- (IBAction)host_return:(id)sender;
- (IBAction)ExitHost:(id)sender;

- (IBAction)define_delay:(id)sender;
@property (weak) IBOutlet NSSlider *delay_bar;
@property (weak) IBOutlet NSTextField *delay_txt;

- (IBAction)Start_button:(id)sender;
- (IBAction)Stop_button:(id)sender;

- (IBAction)Log_check_action:(id)sender;
@property (weak) IBOutlet NSButton *Logs_check_display;
@property (weak) IBOutlet NSTextField *logs_display;

@end

