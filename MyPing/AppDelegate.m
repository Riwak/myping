//
//  AppDelegate.m
//  MyPing
//
//  Created by Audouin d'Aboville on 20/11/2015.
//  Copyright © 2015 Audouin d'Aboville. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
