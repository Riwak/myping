//
//  ViewController.m
//  MyPing
//
//  Created by Audouin d'Aboville on 20/11/2015.
//  Copyright © 2015 Audouin d'Aboville. All rights reserved.
//

#import "ViewController.h"
#import "SimplePing.h"

@interface SimplePingClient : NSObject<SimplePingDelegate>

+(void)pingHostname:(NSString*)hostName andResultCallback:(void(^)(NSString* latency))result;

@end

@interface SimplePingClient()
{
    SimplePing* _pingClient;
    NSDate* _dateReference;
}

@property(nonatomic, strong) void(^resultCallback)(NSString* latency);

@end

@implementation SimplePingClient

+(void)pingHostname:(NSString*)hostName andResultCallback:(void(^)(NSString* latency))result
{
    static SimplePingClient* singletonPC = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonPC = [[SimplePingClient alloc] init];
    });
    
    //ping hostname
    [singletonPC pingHostname:hostName andResultCallBlock:result];
}

-(void)pingHostname:(NSString*)hostName andResultCallBlock:(void(^)(NSString* latency))result
{
    _resultCallback = result;
    _pingClient = [SimplePing simplePingWithHostName:hostName];
    _pingClient.delegate = self;
    [_pingClient start];
}

#pragma mark - SimplePingDelegate methods
- (void)simplePing:(SimplePing *)pinger didStartWithAddress:(NSData *)address
{
    [pinger sendPingWithData:nil];
}

- (void)simplePing:(SimplePing *)pinger didFailWithError:(NSError *)error
{
    _resultCallback(nil);
}

- (void)simplePing:(SimplePing *)pinger didSendPacket:(NSData *)packet
{
    _dateReference = [NSDate date];
}

- (void)simplePing:(SimplePing *)pinger didFailToSendPacket:(NSData *)packet error:(NSError *)error
{
    [pinger stop];
    _resultCallback(nil);
}

- (void)simplePing:(SimplePing *)pinger didReceivePingResponsePacket:(NSData *)packet
{
    [pinger stop];
    NSDate *end=[NSDate date];
    double latency = [end timeIntervalSinceDate:_dateReference] * 1000;//get in miliseconds
    _resultCallback([NSString stringWithFormat:@"%.f", latency]);
}

- (void)simplePing:(SimplePing *)pinger didReceiveUnexpectedPacket:(NSData *)packet
{
    [pinger stop];
    _resultCallback(nil);
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Mise en place et récupération du fichier de sauvegarde
    NSString *contenu;
    NSString *contenu2;
    NSString *chemin = [[self applicationDocumentsDirectory].path
                           stringByAppendingPathComponent:@"data.txt"];
    NSString *chemin2 = [[self applicationDocumentsDirectory].path
                        stringByAppendingPathComponent:@"data2.txt"];
    contenu = [NSString stringWithContentsOfFile:chemin encoding:NSUTF8StringEncoding error:nil];
    contenu2 = [NSString stringWithContentsOfFile:chemin2 encoding:NSUTF8StringEncoding error:nil];
    
    if (contenu2 == NULL || contenu == NULL) // Cas ou le fichier de sauvegarde est inexistant.
    {
        NSString* texte = @"1";
        NSString *path = [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:@"data.txt"];
        [texte writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"data upgrade (bool), %@", texte);
        delay_value = 1;
        
        texte = @"google.com";
        path = [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:@"data2.txt"];
        [texte writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"data upgrade (bool), %@", texte);
        Host = @"google.com";
    }
    else // Récupération de la sauvegarde.
    {
        double tempory_delay_value = [contenu doubleValue];
        NSString *tempory_host = contenu2;
        
        NSLog(@"%f", tempory_delay_value);
        NSLog(@"%@", tempory_host);
        
        // Vérification du fichier de sauvegarde
        if (tempory_delay_value>=0.5 && tempory_delay_value<=5)
        {
            delay_value = tempory_delay_value;
        }
        else
        {
            delay_value = 1;
        }
        
        // Pas de vérification necessaire pour le host
        
        Host = tempory_host;
    }
    
    // Initialisation de l'app.
    _host.stringValue = Host;
    _delay_bar.doubleValue = delay_value;
    _delay_txt.stringValue = [NSString stringWithFormat:@"%.1f", delay_value];
    _delay_txt.stringValue  = [ _delay_txt.stringValue stringByAppendingString:@" s"];

    // Initialisation du menubar. [*]
    _statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    _statusItem.title = @"Off";
    NSMenu *menu = [[NSMenu alloc] init];
    [menu addItemWithTitle:@"Open MyPing" action:@selector(open:) keyEquivalent:@"A"];
    [menu addItem:[NSMenuItem separatorItem]];
    [menu addItemWithTitle:@"Start Ping" action:@selector(Start_button:) keyEquivalent:@"B"];
    [menu addItemWithTitle:@"Stop Ping" action:@selector(Stop_button:) keyEquivalent:@"C"];
    [menu addItem:[NSMenuItem separatorItem]];
    [menu addItemWithTitle:@"Quit MyPing" action:@selector(terminate:) keyEquivalent:@"D"];
    _statusItem.menu = menu;
    
    // Lancement automatique du pinger avec les paramètres de sauvegarde.
    [Timer invalidate];
    Timer = nil;
    _logs_display.stringValue = @"Logs : Wait";
    _statusItem.title = @"Wait";
    Timer = [NSTimer scheduledTimerWithTimeInterval:delay_value target:self selector:@selector(MAINCONTROLLER:) userInfo:nil repeats:YES];
}

- (void)setRepresentedObject:(id)representedObject
{
    [super setRepresentedObject:representedObject];
}

- (IBAction)Start_button:(id)sender
{
    // Clean + Ouverture de la boucle main.
    [Timer invalidate];
    Timer = nil;
    _logs_display.stringValue = @"Logs : Wait";
    _statusItem.title = @"Wait";
    
    Timer = [NSTimer scheduledTimerWithTimeInterval:delay_value target:self selector:@selector(MAINCONTROLLER:) userInfo:nil repeats:YES];
}

- (void)MAINCONTROLLER:(id)timer
{
    [SimplePingClient pingHostname:Host
                 andResultCallback:^(NSString *latency) {
                     
                     if (latency==NULL)
                     {
                         _statusItem.title = @"TimeOut";
                         _statusItem.image = [NSImage imageNamed:@"feedbin-logo"];
                         
                         if (_Logs_check_display.state==1)
                         {
                             _logs_display.stringValue = @"TimeOut";
                         }
                         else
                         {
                             _logs_display.stringValue = @"Logs : Empty";
                         }
                     }
                     else
                     {
                         latency = [latency stringByAppendingString:@" ms"];
                     
                         NSLog(@"your latency is: %@", latency);
                     
                         _statusItem.title = latency;
                         
                         if (_Logs_check_display.state==1)
                         {
                             _logs_display.stringValue = @"Your latency is: ";
                             _logs_display.stringValue  = [ _logs_display.stringValue stringByAppendingString:latency];
                         }
                         else
                         {
                             _logs_display.stringValue = @"Logs : Empty";
                         }
                     }
                 }];
}

- (IBAction)Stop_button:(id)sender
{
    // Fermeture de la boucle main
    [Timer invalidate];
    Timer = nil;
     _logs_display.stringValue = @"Logs : Empty";
    _statusItem.title = @"Off";
}

- (IBAction)Log_check_action:(id)sender
{
    
}

- (IBAction)host_return:(id)sender
{
    Host = [_host stringValue];
    NSLog(@"%@%@", @"Vous avez tapé : ",Host);
    
    // Mise à jour de la sauvegarde
    NSString* texte = Host;
    NSString *path = [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:@"data2.txt"];
    [texte writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"data upgrade (bool), %@", texte);
}

- (IBAction)ExitHost:(id)sender
{
    [sender resignFirstResponder];
}

- (IBAction)define_delay:(id)sender
{
    // Fermeture de la boucle main
    [Timer invalidate];
    Timer = nil;
    _logs_display.stringValue = @"Logs : Wait";
    _statusItem.title = @"Off";
    
    // Affichage du changement de valeur
    delay_value = _delay_bar.doubleValue;
    _delay_txt.stringValue = [NSString stringWithFormat:@"%.1f", delay_value];
    _delay_txt.stringValue  = [ _delay_txt.stringValue stringByAppendingString:@" s"];
    
    // Mise à jour de la sauvegarde
    NSString* texte = [NSString stringWithFormat:@"%.1f", delay_value];
    NSString *path = [[self applicationDocumentsDirectory].path stringByAppendingPathComponent:@"data.txt"];
    [texte writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"data upgrade (bool), %@", texte);
    
    // Initialisation de la nouvelle boucle de déroulement
    Timer = [NSTimer scheduledTimerWithTimeInterval:delay_value target:self selector:@selector(MAINCONTROLLER:) userInfo:nil repeats:YES];
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}

@end
